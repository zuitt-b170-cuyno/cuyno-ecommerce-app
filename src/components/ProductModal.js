import { useEffect, useState } from 'react';
import { Modal, Button, Col } from 'react-bootstrap'
import NumericInput from 'react-numeric-input'
import Swal from 'sweetalert2'
import * as AiIcons from 'react-icons/ai'
import * as MdIcons from 'react-icons/md'

function ProductModal(props) {
    const { show, handleClose, productId } = props
    const [ product, setProduct ] = useState([])
    const [ quantity, setQuantity ] = useState(1)
    const [ total, setTotal ] = useState(0)

    const buy = () => {
        fetch('https://vast-mountain-07825.herokuapp.com/api/orders/checkout', {
            method: 'POST',
            headers: { 
                'Content-Type': 'application/json', 
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            },
            body: JSON.stringify({
                productId: productId,
                price: price,
                quantity: quantity
            })
        })
        .then(res => res.json())
        .then(result => {
            if (result) {
                Swal.fire({
                    title: "Purchase Successful",
                    icon: "success",
                    text: `Thank you for buying ${name}`
                }).then(() => {
                    handleClose()
                    setQuantity(1)
                })
            }
        })
    }

    const addToCart = () => {
        fetch('https://vast-mountain-07825.herokuapp.com/api/carts/add', {
            method: 'POST',
            headers: { 
                'Content-Type': 'application/json', 
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            },
            body: JSON.stringify({
                product: productId,
                quantity: quantity
            })
        })
        .then(res => res.json())
        .then(result => {
            console.log(result)
            if (result) {
                Swal.fire({
                    title: `Add To Cart`,
                    icon: "success",
                    text: `${name} has been added to the cart`
                }).then(() => {
                    handleClose()
                    setQuantity(1)
                })
            }
        })
    }

    const { name, price, description } = product
    useEffect(() => {
        const total = price * quantity
        setTotal(total)

        fetch(`https://vast-mountain-07825.herokuapp.com/api/products/${productId}`)
            .then(res => res.json())
            .then(product => setProduct(product))
    }, [ quantity, productId, price ])

    return (
        <Modal
            show={ show }
            onHide={ handleClose }
            backdrop="static"
            keyboard={ false }
        >
            <Modal.Header closeButton>
                <Modal.Title>
                    <h3 className='font-weight-bold'>{ name }</h3>
                    <p className='text-muted small mb-0'>Price: P{ price }.00</p>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <h5 className='font-weight-bold'>Description</h5>
                <p>{ description }</p>
                <p className='mb-2 text-muted'>Quantity:</p>
                <p>
                    <NumericInput 
                        className="form-control mb-n3 small"
                        min={ 1 } 
                        max={ 100 } 
                        value={ quantity }
                        mobile
                        onChange={ value => setQuantity(Number(value)) }
                    />
                </p>
                <p className='text-muted mt-4 mb-0'>Total Amount: P{ total }.00</p>
            </Modal.Body>
            <Modal.Footer>
                <Col>
                    <Button 
                        variant="secondary" 
                        onClick={ handleClose }
                        className='ml-n3'
                    >
                        <MdIcons.MdOutlineCancel
                            style={{ fontSize: '1.3rem' }} 
                            className='mr-2'
                        />
                        Cancel
                    </Button>
                </Col>
                <Button 
                    variant="primary"
                    onClick={ addToCart }
                >
                    <AiIcons.AiOutlineShoppingCart 
                        style={{ fontSize: '1.3rem' }} 
                        className='mr-2'
                    /> 
                    Add to Cart
                </Button>
                <Button 
                    variant="success"
                    onClick={ buy }
                >
                    <AiIcons.AiOutlinePlusCircle
                        style={{ fontSize: '1.3rem' }} 
                        className='mr-2'
                    /> 
                    Buy Item
                </Button>
            </Modal.Footer>
        </Modal>
    )
}

export default ProductModal