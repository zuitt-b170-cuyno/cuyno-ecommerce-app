import React from 'react'
import * as Ailcons from 'react-icons/ai'
import * as Rilcons from 'react-icons/ri'

export const SidebarData = [
    {
        id: 'homepage',
        title: 'Homepage',
        path: '/',
        icon: <Ailcons.AiOutlineHome />,
        loggedIn: false,
        adminFeature: true
    },
    {
        id: 'login',
        title: 'Login',
        path: '/login',
        icon: <Ailcons.AiOutlineLogin />,
        loggedIn: false,
        adminFeature: true
    },
    {
        id: 'register',
        title: 'Register',
        path: '/register',
        icon: <Rilcons.RiRegisteredLine />,
        loggedIn: false,
        adminFeature: true
    },
    {
        id: 'product',
        title: 'Product',
        path: '/product',
        icon: <Rilcons.RiProductHuntLine />,
        loggedIn: true,
        adminFeature: true
    },
    {
        id: 'cart',
        title: 'Cart',
        path: '/cart',
        icon: <Ailcons.AiOutlineShoppingCart />,
        loggedIn: true,
        adminFeature: false
    },
    {
        id: 'order',
        title: 'Order History',
        path: '/order',
        icon: <Rilcons.RiListOrdered />,
        loggedIn: true,
        adminFeature: true
    },
    {
        id: 'logout',
        title: 'Logout',
        path: '/logout',
        icon: <Rilcons.RiLogoutCircleLine />,
        loggedIn: true,
        adminFeature: true
    }
]