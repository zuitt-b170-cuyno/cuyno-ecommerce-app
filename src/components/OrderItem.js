import React from 'react'

function OrderItem(props) {
    const { order, isAdmin } = props
    const { product, totalAmount, purchasedOn, user } = order
    const { name, price } = product
    const { firstName, lastName } = user
    const quantity = Math.round(totalAmount / price)
    const [ date, time ] = purchasedOn.split('T')
    const simplifiedTime = time.slice(0, 8) 

    return (
        <tr>
            <td>{ name }</td>
            {
                !isAdmin ? null :
                <td width='170'>{ firstName } { lastName }</td>
            }
            <td width='170'>P{ price }.00</td>
            <td width='100'>{ quantity }</td>
            <td width='170'>P{ totalAmount }.00</td>
            <td width='170'>{ date } { simplifiedTime }</td>
        </tr>
    )
}

export default OrderItem