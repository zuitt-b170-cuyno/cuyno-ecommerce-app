import { Modal, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'

function LoginModal(props) {
    const { show, handleClose } = props

    return (
        <Modal
            show={ show }
            onHide={ handleClose }
            backdrop="static"
            keyboard={ false }
        >
            <Modal.Header closeButton>
                <Modal.Title>
                    <h3 className='font-weight-bold'>Login</h3>
                    <p className='text-muted small mb-0'>You cannot proceed without logging in first</p>
                </Modal.Title>
            </Modal.Header>
            <Modal.Footer>
                <Link 
                    className="btn btn-success btn-block" 
                    to="/login"
                >
                    Log in to Proceed
                </Link>
            </Modal.Footer>
        </Modal>
    );
}

export default LoginModal