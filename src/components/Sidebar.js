import React, { Fragment, useState, useContext } from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import * as AiIcons from 'react-icons/ai'
import * as FaIcons from 'react-icons/fa'
import { SidebarData } from './SidebarData'
import SubMenu from './SubMenu'
import { IconContext } from 'react-icons/lib'
import UserContext from '../UserContext'

const Nav = styled.div
`
    background: #15171c;
    height: 80px;
    display: flex;
    align-items: center;
`
const NavIcon = styled(Link)
`
    margin-left: 2rem;
    font-size: 2rem;
    height: 80px;
    display: flex;
    justify-content: flex-start;
    align-items: center;
`

const SidebarNav = styled.nav
`
    background: #15171c;
    width: 250px;
    height: 100vh;
    display: flex;
    justify-content: center;
    position: fixed;
    top: 0;
    left: ${({ sidebar }) => (sidebar ? '0' : '-100%')};
    transition: 350ms;
    z-index: 10;
`

const SidebarWrap = styled.nav
`
    width: 100%;
`

function Sidebar() {
    const [ sidebar, setSidebar ] = useState(false)
    const showSidebar = () => setSidebar(!sidebar)
    const { user } = useContext(UserContext)
    const { isAdmin } = user

    return (
        <Fragment>
            <IconContext.Provider value={{ color: '#fff' }} >
                <Nav>
                    <NavIcon to='#'>
                        <FaIcons.FaBars onClick={ showSidebar } />
                    </NavIcon>
                </Nav>
                <SidebarNav sidebar={ sidebar }>
                    <SidebarWrap>
                        <NavIcon to='#'>
                            <AiIcons.AiOutlineClose onClick={ showSidebar } />             
                        </NavIcon>
                        { 
                            isAdmin ?
                            SidebarData
                                .filter(item => !localStorage.getItem('token') ? !item.loggedIn : item.loggedIn)
                                .filter(item => isAdmin ? item.adminFeature : !item.adminFeature)
                                .map((item, index) => <SubMenu item={ item } key={ index } />) :
                            SidebarData
                            .filter(item => !localStorage.getItem('token') ? !item.loggedIn : item.loggedIn)
                            .map((item, index) => <SubMenu item={ item } key={ index } />)    
                        }
                    </SidebarWrap>
                </SidebarNav>
            </IconContext.Provider>
        </Fragment>
    )
}

export default Sidebar