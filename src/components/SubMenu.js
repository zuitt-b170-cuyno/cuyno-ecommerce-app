import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

const SidebarLink = styled(Link)
`
    display: flex;
    color: '#e1e9fc';
    justify-content: space-between;
    align-items: center;
    padding: 20px;
    list-style: none;
    height: 60px;
    text-decoration: none;
    font-size: 20px;
    
    &:hover {
        background: #252831;
        border-left: 4px solid #632ce4;
        cursor: pointer;
    }
`

const SidebarLabel = styled.span
`
    margin-left: 16px;
`

const SubMenu = ({ item }) => {
    const [ user, setUser ] = useState('')

    useEffect(() => {
        fetch(`https://vast-mountain-07825.herokuapp.com/api/users/details`, {
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(user => {
            const { _id: userId, isAdmin } = user

            if (!isAdmin && item.path === '/order')
                item.path += `/${userId}`
        })
    }, [ item ])

    return (
        <>
            <SidebarLink to={ item.path } id={ item.id }>
                <div>
                    { item.icon }
                    <SidebarLabel>{ item.title }</SidebarLabel>
                </div>
            </SidebarLink>
        </>
    )
}

export default SubMenu