import { Fragment, useState, useContext } from 'react'
import { Card, Button } from 'react-bootstrap'
import ProductModal from './../components/ProductModal'
import LoginModal from './../components/LoginModal'
import UserContext from './../UserContext'
import UpdateModal from './../components/UpdateModal'
import * as MdIcons from 'react-icons/md'
import * as VscIcons from 'react-icons/vsc'
import * as AiIcons from 'react-icons/ai'
import * as CgIcons from 'react-icons/cg'

function ProductCard({ product }) {   
    const { _id, name, description, price, image, isActive } = product
    const [ show, setShow ] = useState(false)
    const [ activate, setActivate ] = useState(isActive)
    const { user } = useContext(UserContext)

    const handleShow = () => setShow(true)
    const handleClose = () => setShow(false)
    const handleActivate = () => setActivate(!activate)

    const [ update, setUpdate ] = useState(false)
    const showUpdate = () => setUpdate(true)
    const closeUpdate = () => setUpdate(false)

    const modal = !localStorage.getItem('token') ? 
        <LoginModal 
            show={ show }
            handleClose={ handleClose }
            productId={ _id }
        /> :
        <ProductModal 
            show={ show }
            handleClose={ handleClose }
            productId={ _id }
        />

    const activateButton = !activate ? 
    <Button
        variant="success" 
        onClick={ () => {
            handleActivate()
            fetch(`https://vast-mountain-07825.herokuapp.com/api/products/activate/${_id}`, {
                method: 'PUT',
                headers: {
                    Authorization: `Bearer ${ localStorage.getItem('token') }`
                } 
            })
        }}
    >
        <AiIcons.AiOutlineCheckCircle
            style={{ fontSize: '1.3rem' }} 
            className='mr-2'
        />
        Activate
    </Button> :
    <Button
        variant="danger" 
        onClick={ () => {
            handleActivate()
            fetch(`https://vast-mountain-07825.herokuapp.com/api/products/archive/${_id}`, {
                method: 'PUT',
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                } 
            })
        }}
    >
        <VscIcons.VscError
            style={{ fontSize: '1.3rem' }} 
            className='mr-2'
        />
        Deactivate
    </Button>

    const buttonGroup = user.isAdmin ? 
    <Fragment>
        <Button
            variant="primary" 
            onClick={ showUpdate }
            className='mr-2'
        >
            <MdIcons.MdUpdate
                style={{ fontSize: '1.3rem' }} 
                className='mr-2'
            />
            Update
        </Button>
        { activateButton }
    </Fragment> :
    <Button 
        variant="success" 
        onClick={ handleShow }
    >
        <CgIcons.CgEnter
            style={{ fontSize: '1.3rem' }} 
            className='mr-2'
        />
        Proceed
    </Button>

    return (
        <Fragment>
            <Card style={{ width: '20rem' }} className='mr-2'>
                <Card.Img 
                    variant="top" 
                    src={ image }
                    style={{
                        height: '15rem',
                        width: '20rem'
                    }}
                />
                <Card.Body>
                    <Card.Title className='font-weight-bold'>{ name }</Card.Title>
                    <Card.Text><b>Price:</b> P{ price }.00</Card.Text>
                    <Card.Text>{ description }</Card.Text>
                    { buttonGroup }
                </Card.Body>
                { modal }
                <UpdateModal
                    update={ update }
                    closeUpdate={ closeUpdate }
                    productId={ _id }
                />
            </Card>
        </Fragment>
    )
}

export default ProductCard