import React from 'react'
import { Button } from 'react-bootstrap'
import Swal from 'sweetalert2'

function CartItem({ cart }) {
    const { quantity, product } = cart
    const { _id: productId, name, price } = product
    const subTotal = quantity * price

    const buy = () => {
        fetch('https://vast-mountain-07825.herokuapp.com/api/orders/checkout', {
            method: 'POST',
            headers: { 
                'Content-Type': 'application/json', 
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            },
            body: JSON.stringify({
                productId: productId,
                price: price,
                quantity: quantity
            })
        })
        .then(res => res.json())
        .then(result => {
            if (result) {
                fetch('https://vast-mountain-07825.herokuapp.com/api/carts/purchase', {
                    method: 'PUT',
                    headers: { 
                        'Content-Type': 'application/json', 
                        Authorization: `Bearer ${ localStorage.getItem('token') }`
                    },
                    body: JSON.stringify({ product: productId })
                })
                .then(() => {
                    Swal.fire({
                        title: "Purchase Successful",
                        icon: "success",
                        text: `Thank you for buying ${name}`
                    }).then(() => window.location.reload())
                })
            }
        })
    }

    const remove = () => {
        fetch('https://vast-mountain-07825.herokuapp.com/api/carts/remove', {
            method: 'PUT',
            headers: { 
                'Content-Type': 'application/json', 
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            },
            body: JSON.stringify({ product: productId })
        }).then(() => window.location.reload())
    }

    return (
        <tr>
            <td>{ name }</td>
            <td width='170'>P{ price }.00</td>
            <td width='100'>{ quantity }</td>
            <td width='170'>P{ subTotal }.00</td>
            <th width='170'>
                <Button 
                    variant="outline-success" 
                    className='mr-2'
                    onClick={ buy }
                >
                    Buy
                </Button>
                <Button 
                    variant="outline-danger"
                    onClick={ remove }
                >
                    Remove
                </Button>
            </th>
        </tr>
    )
}

export default CartItem