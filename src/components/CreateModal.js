import { useEffect, useState } from 'react';
import { Modal, Button, Col, Form, Row } from 'react-bootstrap'
import Swal from 'sweetalert2'
import * as AiIcons from 'react-icons/ai'
import * as MdIcons from 'react-icons/md'

function CreateModal(props) {
    const { create, closeCreate } = props
    const [ name, setName ] = useState('')
    const [ description, setDescription ] = useState('')
    const [ image, setImage ] = useState('')
    const [ price, setPrice ] = useState('')
    const [ active, setActive ] = useState(true)

    const createProduct = event => {
        event.preventDefault()

        fetch('https://vast-mountain-07825.herokuapp.com/api/products/create', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json', 
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price,
                image: image
            })
        })
        .then(res => res.json())
        .then(res => {
            if (res) {
                Swal.fire({
                    title: "Food Added Successfully",
                    icon: "success",
                    text: `${ name } has been added successfully`
                })
                .then(() => {
                    closeCreate()
                    setName('')
                    setDescription('')
                    setPrice('')
                    setImage('')
                })
                .then(() => window.location.reload())
            }
        })
    }

    useEffect(() => {
        const isActive = !(name && description && price && image)
        setActive(isActive)
    }, [ name, description, price, image ])

    return (
        <Modal
            show={ create }
            onHide={ closeCreate }
            backdrop="static"
            keyboard={ false }
        >
            <Modal.Header closeButton>
                <Modal.Title className='mx-2'>
                    <h3 className='font-weight-bold'>Create New Food Item</h3>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Row className='mx-2'>
                        <Col className='p-0'>
                            <Form.Group className='mt-n2'>
                                <Form.Label>Name</Form.Label>
                                <Form.Control 
                                    type="text" 
                                    placeholder="Enter the name of food item"
                                    value={ name }
                                    required
                                    onChange={ event => setName(event.target.value) }
                                />
                            </Form.Group>
                            <Form.Group className='mt-n2'>
                                <Form.Label>Description</Form.Label>
                                <Form.Control 
                                    placeholder="Enter description"
                                    as="textarea" 
                                    rows={3} 
                                    value={ description }
                                    required
                                    onChange={ event => setDescription(event.target.value) }
                                />
                            </Form.Group>
                            <Form.Group className='mt-n2'>
                                <Form.Label>Price</Form.Label>
                                <Form.Control 
                                    type="number" 
                                    placeholder="Enter the price"
                                    value={ price }
                                    required
                                    onChange={ event => setPrice(event.target.value) }
                                />
                            </Form.Group>
                            <Form.Group className='mt-n2 mb-0'>
                                <Form.Label>Image Link</Form.Label>
                                <Form.Control 
                                    placeholder="Paste the image link"
                                    as="textarea" 
                                    rows={3} 
                                    value={ image }
                                    required
                                    onChange={ event => setImage(event.target.value) }
                                />
                            </Form.Group>
                        </Col>
                    </Row>
                </Form>
            </Modal.Body>
            <Modal.Footer className='mx-2'>
                <Col>
                    <Button 
                        variant="secondary" 
                        onClick={ closeCreate }
                        className='ml-n3'
                    >
                        <MdIcons.MdOutlineCancel
                            style={{ fontSize: '1.3rem' }} 
                            className='mr-2'
                        />
                        Cancel
                    </Button>
                </Col>
                <Button 
                    id='create-button'
                    variant='success'
                    disabled={ active }
                    onClick={ event => createProduct(event) }
                >
                    <AiIcons.AiOutlinePlusCircle
                        style={{ fontSize: '1.3rem' }} 
                        className='mr-2'
                    /> 
                    Add New Food Item
                </Button>
            </Modal.Footer>
        </Modal>
    )
}

export default CreateModal