import React, { useState, useEffect } from 'react'
import Sidebar from './components/Sidebar'
import { BrowserRouter as Router } from 'react-router-dom'
import { Routes, Route } from 'react-router-dom'
import Catalog from './pages/Catalog'
import Cart from './pages/Cart'
import Order from './pages/Order'
import OrderAdmin from './pages/OrderAdmin'
import Homepage from './pages/Homepage'
import Login from './pages/Login'
import Register from './pages/Register'
import Logout from './pages/Logout'
import { UserProvider } from './UserContext'

function App() {
    const [ user, setUser ] = useState({ id: null, isAdmin: null })
    const unsetUser = () => {
		localStorage.clear();
		setUser({ id: null, isAdmin: null });
    }

    useEffect(() => {
        fetch(`http://localhost:4000/api/users/details`, {
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(user => {
            const { _id: userId, isAdmin } = user
            const token = userId ? { id: userId, isAdmin: isAdmin } : { id: null, isAdmin: null }
            setUser(token)
		})
    }, [])

    return (
            <UserProvider value={{ user, setUser, unsetUser }}>
                <Router>
                    <Sidebar />
                    <Routes>
                        <Route path='/' element={ <Homepage /> } />
                        <Route path='/login' element={ <Login /> } />
                        <Route path='/register' element={ <Register /> } />
                        <Route path='/product' element={ <Catalog /> } />
                        <Route path='/order' element={ <OrderAdmin /> } />
                        <Route path='/order/:id' element={ <Order /> } />
                        <Route path='/cart' element={ <Cart /> } />
                        <Route path='/logout' element={ <Logout /> } />
                    </Routes>
                </Router>
            </UserProvider>
    )
}

export default App
