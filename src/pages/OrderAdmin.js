import React, { useEffect, useState, useContext } from 'react'
import { Container, Table, Row } from 'react-bootstrap'
import OrderItem from './../components/OrderItem'
import UserContext from '../UserContext'

function OrderAdmin() {
    const [ order, setOrder ] = useState([])
    const { user } = useContext(UserContext)
    const { isAdmin } = user

    useEffect(() => {
        fetch(`https://vast-mountain-07825.herokuapp.com/api/orders`, {
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
            .then(res => res.json())
            .then(order => setOrder(order))
    }, [])

    const OrderItems = order.map((order, index) => <OrderItem order={ order } key={ index } isAdmin={ isAdmin } />)
    
    return (
        <Container fluid>
            <Row className='d-flex justify-content-center my-4'>
                <h1>Customer's Order History</h1>
            </Row>
            <Row className='d-flex justify-content-center'>
                <Table 
                    striped 
                    bordered 
                    hover 
                    style={{ width: '60rem' }}
                >
                    <thead>
                        <tr>
                            <th>Food Name</th>
                            <th>Purchaser Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Total Amount</th>
                            <th>Purchased On</th>
                        </tr>
                    </thead>
                    <tbody>
                        { OrderItems }
                    </tbody>
                </Table>
            </Row>
        </Container>
    )
}

export default OrderAdmin