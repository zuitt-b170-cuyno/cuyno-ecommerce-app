import React, { useState, useEffect, useContext, Fragment } from 'react'
import { Container, Row, Button } from 'react-bootstrap'
import ProductCard from './../components/ProductCard'
import UserContext from '../UserContext'
import * as AiIcons from 'react-icons/ai'
import CreateModal from './../components/CreateModal'

function Catalog() {
    const [ product, setProduct ] = useState([])
    const [ create, setCreate ] = useState(false)
    const { user } = useContext(UserContext)

    const showCreate = () => setCreate(true)
    const closeCreate = () => setCreate(false)

    useEffect(() => {
        if (user.isAdmin) {
            fetch('https://vast-mountain-07825.herokuapp.com/api/products/')
                .then(res => res.json())
                .then(products => setProduct(products))
        }
        else {
            fetch('https://vast-mountain-07825.herokuapp.com/api/products/active')
                .then(res => res.json())
                .then(products => setProduct(products))
        }
    }, [ user ])

    const ProductCards = product.map(product => <ProductCard product={ product } key={ product._id } />)
    
    return (
        <Container fluid>
            <Row className='d-flex justify-content-center'>
                <h1 className='my-4'>Our Specialty Food Products</h1>
            </Row>
            <Row className='d-flex justify-content-center'>
                {
                    !user.isAdmin ? null : 
                    <Fragment>
                        <Button 
                            variant="success"
                            className='mb-3'
                            style={{ width: '20rem' }} 
                            onClick={ showCreate }
                        >
                            <AiIcons.AiOutlinePlusCircle
                                style={{ fontSize: '1.3rem' }} 
                                className='mr-2'  
                            /> 
                            Add Food Item
                        </Button>
                    </Fragment>
                }
                </Row>
            <Row className="d-flex justify-content-center">
                { ProductCards }
            </Row>
            <CreateModal 
                create={ create }
                closeCreate={ closeCreate }
            />
        </Container>
    )
}

export default Catalog