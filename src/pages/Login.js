import React, { useState, useEffect, useContext } from 'react'
import { Container, Col, Row, Image, Form, Button } from 'react-bootstrap'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'
import { useNavigate } from 'react-router-dom';

function Login() {
    const { setUser } = useContext(UserContext)
    const [ email, setEmail ] = useState('')
    const [ password, setPassword ] = useState('')
    const [ active, setActive ] = useState(true)
    const navigate = useNavigate()

    const retrieveUserDetails = token => {
        fetch(`https://vast-mountain-07825.herokuapp.com/api/users/details`, {
            headers: {
                Authorization: `Bearer ${ token }`
            }
        })
        .then(res => res.json())
        .then(user => {
            const { _id: userId, isAdmin } = user
            setUser({ id: userId, isAdmin: isAdmin });
        })
    }

    const login = event => {
        event.preventDefault()

        fetch(`https://vast-mountain-07825.herokuapp.com/api/users/login`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(res => {
            const { access, status } = res

            if (status) {
                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to E-Shop!"
                }).then(() => {
                    localStorage.setItem('token', access)
                    retrieveUserDetails(access)

                    setEmail('')
                    setPassword('')
                    navigate('/product')
                })
            }
            else {
                Swal.fire({
                    title: "Login Failed",
                    icon: "error",
                    text: "Incorrect email or password"
                })
            }
        })
    }

    useEffect(() => {
        const isActive = !(email && password)
        const submitButton = document.querySelector('#submit-button')
        setActive(isActive)
        submitButton.style.cursor = isActive ? "no-drop" : "pointer"
    }, [ email, password ])

    return (
        <Container fluid>
            <Row>
                <Col className='p-0'>
                    <Image 
                        className='col-12 p-0' 
                        src='https://images.pexels.com/photos/2876748/pexels-photo-2876748.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1' 
                        style={{ maxHeight: '87.6vh' }} 
                    />
                </Col>
                <Col className='d-flex flex-column justify-content-center'>
                    <Form 
                        className='px-5'
                        onSubmit={ event => login(event) }
                    >
                        <Form.Group>
                            <Form.Label>Email address</Form.Label>
                            <Form.Control 
                                type="email" 
                                placeholder="Enter email" 
                                value={ email }
                                onChange={ event => setEmail(event.target.value) }
                                required
                            />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Password</Form.Label>
                            <Form.Control 
                                type="password" 
                                placeholder="Enter password" 
                                value={ password }
                                onChange={ event => setPassword(event.target.value) }
                                required
                            />
                        </Form.Group>
                        <Button 
                            id='submit-button'
                            className='col-12' 
                            variant="outline-success" 
                            type="submit"
                            disabled={ active }
                        >
                            Login
                        </Button>
                        <Form.Text 
                            className="text-center mt-3" 
                            style={{ fontSize: "15px" }}
                        >
                            Don't have an account yet? <a href='/register'>Click here</a> to register.
                        </Form.Text>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}

export default Login