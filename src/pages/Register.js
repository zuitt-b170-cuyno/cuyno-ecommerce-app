import React, { useState, useEffect } from 'react'
import { Container, Form, Button, Row, Col } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'
import Swal from 'sweetalert2'

function Register() {
    const [ firstName, setFirstName ] = useState('')
    const [ lastName, setLastName ] = useState('')
    const [ email, setEmail ] = useState('')
    const [ password, setPassword ] = useState('')
    const [ confirmation, setConfirmation ] = useState('')
    const [ active, setActive ] = useState('')

    const [ visible, setVisible ] = useState(true)
    const navigate = useNavigate()

    useEffect(() => {
        const passwordMatch = password === confirmation
        const isActive = !(firstName && lastName && email && password && confirmation && passwordMatch)
        const submitButton = document.querySelector('#submit-button')

        setActive(isActive)
        submitButton.style.cursor = isActive ? "no-drop" : "pointer"
    }, [firstName, lastName, email, password, confirmation])

    const capitalize = text => {
        text = text.trim()
        const words = text.split(" ")
        for (let x in words)
            words[x] = words[x][0].toUpperCase() + words[x].substring(1).toLowerCase()
        
        return words.join(" ")
    }

    const confirmPassword = () => {
        if (!(password && confirmation)) 
            setVisible(true)   
        else 
            password === confirmation ? setVisible(true) : setVisible(false)
    }

    const register = event => {
        event.preventDefault()

        fetch('https://vast-mountain-07825.herokuapp.com/api/users/checkEmail', { 
				method: "POST",
				headers: { "Content-Type": "application/json" },
				body: JSON.stringify({ email: email })
			})
			.then(response => response.json())
			.then(response => {
				if (response)
					Swal.fire('Duplicate Email Found', 'Please provide a different email', 'error')
                else {
					fetch('https://vast-mountain-07825.herokuapp.com/api/users/register', {
						method: "POST",
						headers: { "Content-Type": "application/json" },
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							email: email,
							password: password
						})
					})
					.then(() => {
						Swal.fire('Registered successfully', 'You may now log in.', 'success')
						setFirstName("")
						setLastName("")
						setEmail("")
						setPassword("")
						setConfirmation("")
					})
					.then(() => navigate('/login'))
				}
			})
    }

    return (
        <Container>
            <Col className='d-flex justify-content-center'>
                <Row className='d-flex flex-column align-items-center border border-dark rounded p-5 my-4'>
                    <Form>
                        <Row>
                            <Col className='p-0 mt-n3 mb-1'>
                                <h1 className='font-weight-500'>Register</h1>
                            </Col>
                        </Row>
                        <Row>
                            <Col className='p-0'>
                                <Form.Group>
                                    <Form.Label>First Name</Form.Label>
                                    <Form.Control 
                                        type="email" 
                                        placeholder="Enter first name"
                                        value={ firstName }
                                        required
                                        onChange={ event => setFirstName(event.target.value) }
                                        onBlur={ event => {
                                            const field = capitalize(event.target.value)
                                            setFirstName(field)
                                        }}                                        
                                    />
                                </Form.Group>
                            </Col>
                            <Col className='pr-0'>
                                <Form.Group>
                                    <Form.Label>Last Name</Form.Label>
                                    <Form.Control 
                                        type="email" 
                                        placeholder="Enter last name"
                                        value={ lastName }
                                        required 
                                        onChange={ event => setLastName(event.target.value) }
                                        onBlur={ event => {
                                            const field = capitalize(event.target.value)
                                            setLastName(field)
                                        }}
                                    />
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col className='p-0'>
                                <Form.Group>
                                    <Form.Label>Email address</Form.Label>
                                    <Form.Control 
                                        type="email" 
                                        placeholder="Enter email" 
                                        value={ email }
                                        onChange={ event => setEmail(event.target.value) }
                                        required
                                    />
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col className='p-0'>
                                <Form.Group>
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control 
                                        type="password" 
                                        placeholder="Enter password"
                                        value={ password }
                                        onChange={ event => setPassword(event.target.value) }
                                        onKeyUp={ confirmPassword }
                                        required 
                                    />
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col className='p-0'>
                                <Form.Group>
                                    <Form.Label>Verify Password</Form.Label>
                                    <Form.Control 
                                        type="password" 
                                        placeholder="Confirm password"
                                        value={ confirmation }
                                        onChange={ event => setConfirmation(event.target.value) }
                                        onKeyUp={ confirmPassword }
                                        required 
                                    />
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col className='p-0'>
                                <Form.Text 
                                    className='mt-n3 mb-3'
                                    hidden={ visible }
                                    style={{ 
                                        fontSize: "14px", 
                                        color: "red" 
                                    }}
                                >
                                    Passwords do not match
                                </Form.Text>
                            </Col>
                        </Row>
                        <Row>
                            <Col className='p-0'>
                                <Button 
                                    id='submit-button'
                                    className='col-12' 
                                    variant="outline-success" 
                                    type="submit"
                                    disabled={active}
                                    onClick={ register }
                                >
                                    Register
                                </Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                            <Form.Text 
                                className="text-center mt-3" 
                                style={{ fontSize: "15px" }}
                            >
                                Already have account? <a href='/login'>Click here</a> to login.
                            </Form.Text>
                            </Col>
                        </Row>
                    </Form>
                </Row>
            </Col>
        </Container>
    )
}

export default Register