import React, { useEffect, useState } from 'react'
import { Container, Table, Row} from 'react-bootstrap'
import CartItem from './../components/CartItem'

function Cart() {
    const [ cart, setCart ] = useState([])
    useEffect(() => {
        fetch('https://vast-mountain-07825.herokuapp.com/api/carts/', {
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
            .then(res => res.json())
            .then(cart => setCart(cart))
    }, [])

    const CartItems = cart.map((cart, index) => <CartItem cart={ cart } key={ index } />)
    
    return (
        <Container fluid>
            <Row className='d-flex justify-content-center my-4'>
                <h1>Your Shopping Cart</h1>
            </Row>
            <Row className='d-flex justify-content-center'>
                <Table 
                    striped 
                    bordered 
                    hover 
                    style={{ width: '60rem' }}
                >
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Subtotal</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        { CartItems }
                    </tbody>
                </Table>
            </Row>
        </Container>
    )
}

export default Cart