import React, { Fragment } from 'react'
import Catalog from './Catalog'

function Homepage() {
    return (
        <Fragment>
            <Catalog />
        </Fragment>
    )
}

export default Homepage