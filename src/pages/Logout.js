import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

function Logout() {
    const { unsetUser, setUser } = useContext(UserContext);
    
    useEffect(() => {
        unsetUser()
        setUser({ id: null, isAdmin: null });
    }, [ setUser, unsetUser ])

    return (
        <Navigate to='/' />
    )
}

export default Logout
